load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def include_googletest():
    if "com_google_googletest" not in native.existing_rules():
        http_archive(
            name = "com_google_googletest",
            sha256 = "9dc9157a9a1551ec7a7e43daea9a694a0bb5fb8bec81235d8a1e6ef64c716dcb",
            strip_prefix = "googletest-release-1.10.0",
            urls = ["https://github.com/google/googletest/archive/release-1.10.0.tar.gz"],
        )

def cc_googletest(name, deps = [], out_format = "json", **kwargs):
    native.cc_test(
        name = name,
        deps = deps + ["@com_google_googletest//:gtest"],
        **kwargs
    )
