load("@kcarbon_bazel_libraries//eigen3:include.bzl", "include_eigen3")
load("@kcarbon_bazel_libraries//googletest:include.bzl", "include_googletest")

def include_bazel_libraries():
    include_eigen3()
    include_googletest()
