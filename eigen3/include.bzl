load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def include_eigen3():
    if "com_bitbucket_eigen3" not in native.existing_rules():
        http_archive(
            name = "com_bitbucket_eigen3",
            build_file = "@kcarbon_bazel_libraries//eigen3:BUILD.eigen",
            sha256 = "9f13cf90dedbe3e52a19f43000d71fdf72e986beb9a5436dddcd61ff9d77a3ce",
            strip_prefix = "eigen-eigen-323c052e1731",
            urls = [
                "http://bitbucket.org/eigen/eigen/get/3.3.7.tar.bz2",
            ],
            # patches = ["@kcarbon_bazel_libraries//eigen3:tensor.patch"],
        )
