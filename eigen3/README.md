Taken from https://github.com/tensorflow/tensorflow/blob/4e69e02/third_party/eigen.BUILD

Description:
  Eigen is a C++ template library for linear algebra: vectors,
  matrices, and related algorithms.