#include "Eigen/Core"
#include "Eigen/Geometry"
#include "gtest/gtest.h"

TEST(Eigen, SimpleYaw)
{
  // create an active transform, T_b2a, that rotates the point by 90-deg about Z
  Eigen::Transform<double, 3, Eigen::Affine> T_b2a(Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ()));
  // some vector, r, expressed in frame "b"
  Eigen::Vector3d r_b = Eigen::Vector3d::UnitX();
  // apply the rotation
  auto r_a = T_b2a * r_b;
  // this should be (0, 1, 0)
  // EXPECT_TRUE(EIGEN_MATRIX_EQUAL_DOUBLE(r_a, Eigen::Vector3d::UnitY()));
  double tol = 1e-6;
  EXPECT_NEAR(r_a.x(), 0, tol);
  EXPECT_NEAR(r_a.y(), 1, tol);
  EXPECT_NEAR(r_a.z(), 0, tol);
}

TEST(Eigen, ActiveYawPitch)
{
  // This is an active, extrinsic (aka. fixed-axis) XYZ rotation.
  // Note that in many texts, an XYZ rotation refers to rotating first by X, then Y, then Z.
  // When constructing a matrix, that means you have to multiply by X first, then Y, then Z,
  // which is why the order of matrices then becomes ZYX.
  Eigen::Transform<double, 3, Eigen::Affine> T_b2a(Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ()) *
                                                   Eigen::AngleAxisd(-M_PI_4, Eigen::Vector3d::UnitY()) *
                                                   Eigen::AngleAxisd(0, Eigen::Vector3d::UnitX()));
  // some vector, r, expressed in frame "b"
  Eigen::Vector3d r_b = Eigen::Vector3d::UnitX();
  // apply the rotation
  auto r_a = T_b2a * r_b;

  Eigen::Vector3d truth(0, std::sqrt(2) / 2, std::sqrt(2) / 2);
  double tol = 1e-6;
  EXPECT_NEAR(r_a.x(), truth.x(), tol);
  EXPECT_NEAR(r_a.y(), truth.y(), tol);
  EXPECT_NEAR(r_a.z(), truth.z(), tol);
}

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}